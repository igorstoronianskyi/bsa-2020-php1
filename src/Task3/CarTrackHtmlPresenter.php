<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\Car;
use App\Task1\Track;

/**
 * Class CarTrackHtmlPresenter
 * @package App\Task3
 */
class CarTrackHtmlPresenter
{
    /**
     * @param Track $track
     * @return string
     */
    public function present(Track $track): string
    {
        $cars = $track->all();
        $container = "<div class='container'>";
        foreach ($cars as $car) {
            $container .= '<div class="car">';
            $container .= '<p class="name">' . $car->getName() . '</p>';
            $container .= '<img src="' . $car->getImage() . '">';
            $container .= '<div class="speed">Speed: ' . $car->getSpeed() . ' km/h' . '</div>';
            $container .= '<div class="pitstop">Pit Stop Time: ' . $car->getPitStopTime() . ' seconds' . '</div>';
            $container .= '<div class="consumption">Fuel Consumption: ' . $car->getFuelConsumption() . ' litres' . '</div>';
            $container .= '<div class="volume">Fuel Tank Volume: ' . $car->getFuelTankVolume() . ' litres per 100 km ' . '</div>';
            $container .= '</div>';
        }
        $container .= '</div>';

        return $container;
    }
}
