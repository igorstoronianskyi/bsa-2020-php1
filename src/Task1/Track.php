<?php

declare(strict_types=1);

namespace App\Task1;
/**
 * Class Track
 * @package App\Task1
 */
class Track
{
    private float $lapLength;
    private int $lapsNumber;
    private $cars = [];

    const KMCONSUMPTION = 100;
    const SECONDS = 3600;

    /**
     * Track constructor.
     * @param float $lapLength
     * @param int $lapsNumber
     */
    public function __construct(float $lapLength, int $lapsNumber)
    {
        $this->lapLength = $lapLength;
        $this->lapsNumber = $lapsNumber;
    }

    /**
     * @return float
     */
    public function getLapLength(): float
    {
        return $this->lapLength;
    }

    /**
     * @return int
     */
    public function getLapsNumber(): int
    {
        return $this->lapsNumber;
    }

    /**
     * @param Car $car
     */
    public function add(Car $car): void
    {
        $this->cars[] = $car;
    }

    /**
     * @return array
     */
    public function all(): array
    {
        return $this->cars;
    }

    /**
     * @return Car
     */
    public function run(): Car
    {
        $cars = $this->all();
        $time = [];

        foreach ($cars as $key => $car) {
            $totalLength = $this->lapLength * $this->lapsNumber;
            $speed = $car->getSpeed();
            $pitStopTime = $car->getPitStopTime();
            $fuelConsumption = $car->getFuelConsumption();
            $fuelTankVolume = $car->getFuelTankVolume();

            $pitStop = (($totalLength / self::KMCONSUMPTION) * $fuelConsumption) / $fuelTankVolume;
            $timeAllPitStops = $pitStop * $pitStopTime;
            $timeOfTravel = ($totalLength / self::KMCONSUMPTION) * (self::KMCONSUMPTION / $speed) * self::SECONDS;

            $totalTime = $timeAllPitStops + $timeOfTravel;

            $time[$key] = $totalTime;

        }

        return $this->all()[array_search(min($time), $time)];
    }
}
