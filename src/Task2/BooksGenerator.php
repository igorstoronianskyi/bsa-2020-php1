<?php

declare(strict_types=1);

namespace App\Task2;
/**
 * Class BooksGenerator
 * @package App\Task2
 */
class BooksGenerator
{
    private int $minPagesNumber;
    private int $maxPrice;
    private array $libraryBooks = [];
    private array $storeBooks = [];

    /**
     * BooksGenerator constructor.
     * @param int $minPagesNumber
     * @param int $maxPrice
     * @param array $libraryBooks
     * @param array $storeBooks
     */
    public function __construct(
        int $minPagesNumber,
        array $libraryBooks,
        int $maxPrice,
        array $storeBooks
    )
    {
        $this->minPagesNumber = $minPagesNumber;
        $this->libraryBooks = $libraryBooks;
        $this->maxPrice = $maxPrice;
        $this->storeBooks = $storeBooks;
    }

    /**
     * @return \Generator
     */
    public function generate(): \Generator
    {
        foreach ($this->libraryBooks as $libraryBook) {
            if ($libraryBook->getPagesNumber() >= $this->minPagesNumber) {
                yield $libraryBook;
            }
        }

        foreach ($this->storeBooks as $storeBook) {
            if ($storeBook->getPrice() < $this->maxPrice) {
                yield $storeBook;
            }
        }
    }
}
