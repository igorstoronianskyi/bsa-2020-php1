<?php
require_once __DIR__ . "/../../vendor/autoload.php";

use App\Task2\Book;
use App\Task2\BooksGenerator;

$libraryBooks = [];
$storeBooks = [];

$libraryBooks[] = new Book('Don Quixote', 100, 100);
$libraryBooks[] = new Book('The Great Gatsby', 50, 50);
$libraryBooks[] = new Book('Harry Potter', 20, 20);

$storeBooks[] = new Book('Hamlet', 150, 30);
$storeBooks[] = new Book('Crime and Punishment', 600, 50);
$storeBooks[] = new Book('Pride and Prejudice', 40, 60);


$booksGenerator = new BooksGenerator(15, $libraryBooks, 20, $storeBooks);
